@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    {{ __('You are logged in!') }}
                </div>

                <div class="form-group row">
                    <label for="profile" class="col-md-4 col-form-label text-md-right">{{ __('Profile') }}</label>

                    <div class="col-md-6">
                        <input id="profile" type="file" class="form-control @error('profile') is-invalid @enderror" name="profile" value="{{ old('profile') }}" required autocomplete="profile" autofocus>

                        @error('profile')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name',Auth::user()->name) }}" required autocomplete="name" autofocus>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                    <div class="col-md-6">
                        <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username',Auth::user()->username) }}" required autocomplete="username">

                        @error('username')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email',Auth::user()->email) }}" required autocomplete="email">

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="section" class="col-md-4 col-form-label text-md-right">{{ __('Section') }}</label>

                    <div class="col-md-6">
                        <label for="f1">Front-End</label>
                        <input id="f1" type="radio" class=" @error('section') is-invalid @enderror" name="section" value="frontend" {{Auth::user()->section =='frontend' ? 'checked':''}}>
                        <label for="b1">Back-End</label>
                        <input id="b1" type="radio" class=" @error('section') is-invalid @enderror" name="section" value="backend" {{Auth::user()->section =='backend' ? 'checked':''}}>
                        @error('section')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Interest" class="col-md-4 col-form-label text-md-right">{{ __('Interest') }}</label>

                    <div class="col-md-6">
                        <label for="i1">Food</label>
                        <input id="i1" type="checkbox" name="like[]" value="food" class=" @error('Interest') is-invalid @enderror" name="Interest">
                        <label for="i2">Computer</label>
                        <input id="i2" type="checkbox" name="like[]" value="computer" class=" @error('Interest') is-invalid @enderror" name="Interest">
                        <label for="i3">Support system</label>
                        <input id="i3" type="checkbox" name="like[]" value="support_system" class=" @error('Interest') is-invalid @enderror" name="Interest">
                        <label for="i4">Communication</label>
                        <input id="i4" type="checkbox" name="like[]" value="communication" class=" @error('Interest') is-invalid @enderror" name="Interest">

                        @error('Interest')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="country" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>

                    <div class="col-md-6">
                        <select id="country" class="form-control @error('country') is-invalid @enderror" name="country">
                            <option value="india" {{Auth::user()->country =='india' ? 'selected':''}}>India</option>
                            <option value="australia" {{Auth::user()->country =='australia' ? 'selected':''}}>Australia</option>
                            <option value="canada" {{Auth::user()->country =='canada' ? 'selected':''}}>Canada</option>
                            <option value="london" {{Auth::user()->country =='london' ? 'selected':''}}>London</option>
                            <option value="uk" {{Auth::user()->country =='uk' ? 'selected':''}}>UK</option>
                        </select>
                        @error('country')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="state" class="col-md-4 col-form-label text-md-right">{{ __('State') }}</label>

                    <div class="col-md-6">
                        <select id="state" class="form-control @error('state') is-invalid @enderror" name="state">
                            <option value="gujarat" {{Auth::user()->state =='gujarat' ? 'selected':''}}>gujarat</option>
                            <option value="madhya_pradesh" {{Auth::user()->state =='madhya_pradesh' ? 'selected':''}}>madhya pradesh</option>
                            <option value="hyderabad" {{Auth::user()->state =='hyderabad' ? 'selected':''}}>hyderabad</option>
                            <option value="kerala" {{Auth::user()->state =='kerala' ? 'selected':''}}>kerala</option>
                            <option value="up" {{Auth::user()->state =='up' ? 'selected':''}}>up</option>
                        </select>
                        @error('state')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="city" class="col-md-4 col-form-label text-md-right">{{ __('city') }}</label>

                    <div class="col-md-6">
                        <select id="city" class="form-control @error('city') is-invalid @enderror" name="city">
                            <option value="surat" {{Auth::user()->city =='surat' ? 'selected':''}}>surat</option>
                            <option value="ahmedabad" {{Auth::user()->city =='surat' ? 'selected':''}}>ahmedabad</option>
                            <option value="rajkot" {{Auth::user()->city =='surat' ? 'selected':''}}>rajkot</option>
                            <option value="bharuch" {{Auth::user()->city =='surat' ? 'selected':''}}>bharuch</option>
                            <option value="vadodara" {{Auth::user()->city =='surat' ? 'selected':''}}>vadodara</option>
                        </select>
                        @error('city')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="age" class="col-md-4 col-form-label text-md-right">{{ __('Age') }}</label>

                    <div class="col-md-6">
                        <select id="age" class="form-control @error('age') is-invalid @enderror" name="age">
                            <option value="15" {{Auth::user()->age =='15' ? 'selected':''}}>15</option>
                            <option value="16" {{Auth::user()->age =='16' ? 'selected':''}}>16</option>
                            <option value="17" {{Auth::user()->age =='17' ? 'selected':''}}>17</option>
                            <option value="18" {{Auth::user()->age =='18' ? 'selected':''}}>18</option>
                            <option value="19" {{Auth::user()->age =='19' ? 'selected':''}}>19</option>
                            <option value="20" {{Auth::user()->age =='20' ? 'selected':''}}>20</option>
                        </select>
                        @error('age')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </div>
</div>
@endsection